# Test Project

Small project to test file sharing using the RWTH gitlab servers when using a public repository.

The result is, that when setting a repository to public any outsider can actually take a look into it.
